#include <sys/types.h>  
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(){
    printf("%d \n",fork() && ( fork() || fork() ) );

    return 0;
}
