#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "ex2functions.c"

int a[] = {1,3,6,20,418,12,-1,90,32,12,34,10,91,23,10,1};
int b[]={1,2,4,3,2};

int maxi(int i,int j){
  if (i>j){
    return i;
  }
  return j;
}

int max(int* tab, int debut, int fin){
  int max = tab[debut];
  for (int i = debut+1; i<=fin; i++){
    max = maxi(tab[i],max);
  }
  return max;
}

int maxt(int* tab, int debut, int fin,int seuil){
  if(fin-debut>seuil){
    int m = debut + (fin-debut)/2;
    char f1[20];
    char f2[20];
    sprintf(f1,"%d_a",getpid());
    sprintf(f2,"%d_b",getpid());
    pid_t pid2 ;
    pid_t pid1 = fork(); // creation premier fils
    if (pid1<0){
     printf("fork 1 : erreur\n");
    }else if(pid1==0){//dans le premier fils
      //printf("lancement rec fils1 \n");
      ecrire_fils(maxt(tab,debut,m,seuil),f1);
      exit(1);
    }else if(pid1>0){
      pid2 = fork(); // creation deuxieme fils
      if(pid2<0){
        printf("fork 2 : erreur\n");
      }else if(pid2==0){ // dans le deuxième fils
        //printf("lancement rec fils2 \n");
        ecrire_fils(maxt(tab,m+1,fin,seuil),f2);
        exit(1);
      }// dans le pere
    }
    int status1,status2,a,b;
    //printf("ici le pere\n");
    waitpid(pid2,&status1,0);
    waitpid(pid1,&status2,0);
    lire_pere2(&a,f1);
    lire_pere2(&b,f2);
    //printf("pere fini\n");
    return maxi(a,b);
  }else{ // en dessous du seuil
    //printf("fin recursif\n");
    return max(tab,debut,fin);
  }
}

int main(){
  int res = maxt(a,0,15,2);
  printf("%d\n",res);
  return 0;
}
