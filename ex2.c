#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "ex2functions.c"

int main(){
  int * nb  ;
  int status;
  int pid =fork();
  if (pid<0){
    printf("erreur\n");
  }else if(pid>0){
    waitpid(pid,&status,0);
    lire_pere(nb,"stockage.txt");
    printf("%d \n", *nb);
  }else{
    ecrire_fils(3,"stockage.txt");
    exit(0);
  }
}
