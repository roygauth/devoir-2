#include <sys/types.h> 
#include <unistd.h>     
#include <stdio.h>      
#include <stdlib.h>     

int main()
{
    if (fork() > 0)
          sleep(60);
    else{
          printf("Processus fils\n");
        printf ("PID         : %ld\n", getpid ());
          exit(0);
    }
    return 0;
}
