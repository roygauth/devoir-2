#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "ex2functions.c"


int main(){
  int a,b  ;
  int nb1 =2;
  int nb2 = 6;
  int pid1 =fork();
  if (pid1<0){
    printf("erreur\n");
  }else if(pid1>0){
    int pid2 =fork();
    if (pid2<0){
      printf("erreur\n");
    }else if(pid2==0){//fils 2
      ecrire_fils(nb2,"name2.txt");
      exit(2); //le fils 2 envoie un signal pour pouvoir etre identifié par le père qui l'attend
    }else{//pere
      wait(&a);
      if (WEXITSTATUS(a)==1){
        // cas très majoritaire ou le premier fils a fini en premier
        printf("choix 1\n");
        lire_pere2(&a,"name1.txt");
        wait(&b);
        lire_pere2(&b,"name2.txt");
      }else if(WEXITSTATUS(a)==2){
        //cas rare mais qui se produit ou le deuxième fils a fini en premier
        printf("choix 2\n \n \n \n \n");
        lire_pere2(&a,"name2.txt");
        wait(&b);
        lire_pere2(&b,"name1.txt");
      }
      printf("Nombres: %d %d\n",a,b);
    }
  }else{ //fils 1
    ecrire_fils(nb1,"name1.txt");
    exit(1); //le fils 1 envoie un signal pour pouvoir etre identifié par le père qui l'attend
  }
}
